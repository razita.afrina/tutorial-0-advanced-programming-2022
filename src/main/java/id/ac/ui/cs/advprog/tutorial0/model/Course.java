package id.ac.ui.cs.advprog.tutorial0.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Course {
    public String courseId;
    public String courseName;
    public boolean vacancyStatus;
}
